PROJECT?=unnamed
PROJECT_DIR?=./
DEB_DISTRO?=unstable
INSTALLED_GO?=1.9.4
DEBIAN_FRONTEND=noninteractive
export PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin

clean:
	rm -rf ../*-debs
	rm -rf ../*.deb
	rm -rf ../*.changes
	rm -rf ./obj-*

build-deb-chroot:
	ischroot -t
	sed -i -e "s/DDDDD;/${DEB_DISTRO};/g" $(PROJECT_DIR)/debian/changelog
	. /root/.profile; go env; go version; cd $(PROJECT_DIR); dpkg-buildpackage -us -uc -b

build-rpm-chroot:
	Coming Soon

install-local-chroot-ubuntu:
	sudo apt-get install qemu qemu-user-static qemu-user debootstrap gcc-arm-linux-gnueabihf g++-arm-linux-gnueabihf
	sudo apt-get install gcc-arm-linux-gnueabihf libc6-dev-armhf-cross
	
setup-ubuntu-chroot:
	ischroot -t
	echo "PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin" > /etc/environment
	apt-get install locales tar apt-utils curl tar -y
	locale-gen en_US.UTF-8
	sed -i '/^#.* es_US /s/^#//' /etc/locale.gen
	locale-gen
	apt-get update -y
	apt-get upgrade -y
	
setup-debian-chroot:
	ischroot -t
	echo "PATH=/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin" > /etc/environment
	apt-get install locales tar apt-utils curl tar -y
	echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
	locale-gen
	apt-get update -y
	apt-get upgrade -y

setup-ubuntu-amd64-go-chroot:
	ischroot -t
	echo "deb http://archive.canonical.com/ubuntu/ ${DEB_DISTRO} partner" >> /etc/apt/sources.list
	echo "deb http://us.archive.ubuntu.com/ubuntu/ ${DEB_DISTRO} universe" >> /etc/apt/sources.list
	apt-get update
	curl -fSL "https://golang.org/dl/go${INSTALLED_GO}.linux-amd64.tar.gz" | tar xzC /usr/local
	echo 'GOPATH=/go' >> /root/.bashrc
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /root/.bashrc
	echo 'GOPATH=/go' >> /root/.profile
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /root/.profile
	echo 'GOPATH=/go' >> /etc/bash.bashrc
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /etc/bash.bashrc
	echo 'GOPATH=/go' >> /etc/profile
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /root/profile
	apt-get install apt-utils dpkg-dev dpkg git dh-make devscripts dh-make-golang dh-golang build-essential fakeroot subversion nano -y
	. /root/.profile; go version

setup-ubuntu-armhf-go-chroot:
	ischroot -t
	echo "deb http://ports.ubuntu.com/ubuntu-ports/ ${DEB_DISTRO} partner" >> /etc/apt/sources.list
	echo "deb http://ports.ubuntu.com/ubuntu-ports/ ${DEB_DISTRO} universe" >> /etc/apt/sources.list
	echo "deb http://ports.ubuntu.com/ubuntu-ports/ ${DEB_DISTRO} multiverse" >> /etc/apt/sources.list
	curl -fSL "https://golang.org/dl/go${INSTALLED_GO}.linux-armv6l.tar.gz" | tar xzC /usr/local
	apt-get update
	echo 'GOPATH=/go' >> /root/.bashrc
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /root/.bashrc
	echo 'GOPATH=/go' >> /root/.profile
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /root/.profile
	echo 'GOPATH=/go' >> /etc/bash.bashrc
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /etc/bash.bashrc
	echo 'GOPATH=/go' >> /etc/profile
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /root/profile
	apt-get install apt-utils dpkg-dev dpkg git dh-make devscripts dh-make-golang dh-golang build-essential fakeroot subversion nano -y
	. /root/.profile; go version

setup-debian-amd64-go-chroot:
	ischroot -t
	apt-get update
	curl -fSL "https://golang.org/dl/go${INSTALLED_GO}.linux-amd64.tar.gz" | tar xzC /usr/local
	echo 'GOPATH=/go' >> /root/.bashrc
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /root/.bashrc
	echo 'GOPATH=/go' >> /root/.profile
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /root/.profile
	echo 'GOPATH=/go' >> /etc/bash.bashrc
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /etc/bash.bashrc
	echo 'GOPATH=/go' >> /etc/profile
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /root/profile
	apt-get install apt-utils dpkg-dev dpkg git dh-make devscripts dh-make-golang dh-golang build-essential fakeroot subversion nano -y
	. /root/.profile; go version

setup-debian-armhf-go-chroot:
	ischroot -t
	curl -fSL "https://golang.org/dl/go${INSTALLED_GO}.linux-armv6l.tar.gz" | tar xzC /usr/local
	apt-get update
	echo 'GOPATH=/go' >> /root/.bashrc
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /root/.bashrc
	echo 'GOPATH=/go' >> /root/.profile
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /root/.profile
	echo 'GOPATH=/go' >> /etc/bash.bashrc
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /etc/bash.bashrc
	echo 'GOPATH=/go' >> /etc/profile
	echo 'PATH=/usr/local/go/bin:$$GOPATH/bin:$$PATH' >> /root/profile
	apt-get install apt-utils dpkg-dev dpkg git dh-make devscripts dh-make-golang dh-golang build-essential fakeroot subversion nano -y
	. /root/.profile; go version
